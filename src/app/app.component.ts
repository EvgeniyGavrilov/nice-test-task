import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from "@angular/forms";
import {BehaviorSubject, combineLatest, ReplaySubject, Subject, takeUntil} from "rxjs";
import {Status} from "@enums";
import {DataItem} from "@interfaces";
import {ButtonsListSettings, CallsByCountryMap, Period} from "@types";



export const MOCK_DATA: DataItem[] = [
  {
    agentId: '0',
    timestamp: 1212511511,
    duration: 10,
    country: 'USA',
    status: Status.Active
  },
  {
    agentId: '1',
    timestamp: 1212512512,
    duration: 15,
    country: 'Russia',
    status: Status.Active
  },
  {
    agentId: '2',
    timestamp: 344476353,
    duration: 23,
    country: 'Canada',
    status: Status.Active
  },
  {
    agentId: '3',
    timestamp: 125134983,
    duration: 33,
    country: 'Amsterdam',
    status: Status.Waiting
  },
  {
    agentId: '3',
    timestamp: 125134983,
    duration: 21,
    country: 'Amsterdam',
    status: Status.Waiting
  },
  {
    agentId: '4',
    timestamp: 125134983,
    duration: 24,
    country: 'Amsterdam',
    status: Status.Processed
  },
  {
    agentId: '4',
    timestamp: 125134983,
    duration: 36,
    country: 'Amsterdam',
    status: Status.Processed
  },
  {
    agentId: '5',
    timestamp: 125134983,
    duration: 11,
    country: 'China',
    status: Status['Hang-Up']
  },
  {
    agentId: '5',
    timestamp: 125134983,
    duration: 17,
    country: 'China',
    status: Status['Hang-Up']
  },
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  private _unsubscribe$: Subject<null> = new Subject<null>();
  timeFrame: FormControl<Period> = new FormControl(0, {nonNullable: true})

  buttonsSettings: ButtonsListSettings = [
    {
      isActive: true,
      status: Status["Processed"],
      counter: 0,
    },
    {
      isActive: true,
      status: Status["Active"],
      counter: 0,
    },
    {
      isActive: true,
      status: Status["Waiting"],
      counter: 0,
    },
    {
      isActive: true,
      status: Status["Hang-Up"],
      counter: 0,
    }
  ]
  status: Status[] = [Status.Waiting, Status['Hang-Up'], Status.Processed, Status.Active];

  items$: ReplaySubject<DataItem[]> = new ReplaySubject<DataItem[]>(1);
  activePeriod$: BehaviorSubject<Period> = new BehaviorSubject(0);

  callsByCountry: CallsByCountryMap = new Map<string, DataItem[]>();
  agents: number = 0;

  itemsDataFiltered: DataItem[] = []
  itemsDataOrigin: DataItem[] = []

  constructor() {}

  ngOnInit(): void {
    this.items$.next(MOCK_DATA);
    this.timeFrame.valueChanges.pipe(
      takeUntil(this._unsubscribe$)
    ).subscribe(v => {
      this.activePeriod$.next(v)
    })

    combineLatest([
      this.items$,
      this.activePeriod$,
    ]).pipe(
      takeUntil(this._unsubscribe$)
    ).subscribe((res: [DataItem[], Period]) => {
      const items: DataItem[] = res[0];
      const period: Period = res[1];
      this.itemsDataOrigin = [...this.getItemsByPeriod(items, period)];
      this.recalculateDataByStatus();
    })
  }

  setStatistics() {
    const callsByCountry: CallsByCountryMap = new Map<string, DataItem[]>();
    const agents: Map<string, number> = new Map<string, number>();

    this.itemsDataFiltered.forEach(el => {
      const {agentId, country, status: statusValue, duration, timestamp} = el;

      if(callsByCountry.has(country)) {
        const arr = callsByCountry.get(country);
        arr!.push(el);
        callsByCountry.set(country, arr!)
      } else {
        callsByCountry.set(country, [el])
      }

      if(agents.has(agentId)) {
        const counter = agents.get(agentId);
        agents.set(agentId, counter! + 1)
      } else {
        agents.set(agentId, 1)
      }
    })
    this.callsByCountry = callsByCountry;
    this.agents = agents.size
  }

  getItemsByPeriod(data: DataItem[], period: Period): DataItem[] {
    const oneMin = 60000;
    const oneHour = oneMin * 60;
    let arr =  [];

    if (!data.length) {
      return data;
    }

    if(period !== 0) {
      data.sort((a, b) => b.timestamp - a.timestamp);
      const lastElement = data[0];
      const minTimestamp = lastElement.timestamp - (period * oneHour);
      for(let i = 0; i < data.length - 1; i++) {
        if(data[i].timestamp > minTimestamp) {
          arr.push(data[i])
        } else {
          break
        }
      }
      arr.reverse();
    }

    if(period === 0) {
      data.sort((a, b) => a.timestamp - b.timestamp);
      arr = data;
    }

    return arr
  }

  filterByStatus(item: DataItem): boolean {
    const status: Status[] = [];
    this.buttonsSettings.forEach(el => {
      if(el.isActive) {
        status.push(el.status)
      }
    })
    return status.length ? status.includes(item.status) : false
  }

  recalculateDataByStatus() {
    this.itemsDataFiltered = this.itemsDataOrigin.filter(item => this.filterByStatus(item));
    this.setStatistics()
  }

  getStatusName(status: Status) {
    return Status[status]
  }

  ngOnDestroy(): void {
    this._unsubscribe$.next(null);
    this._unsubscribe$.complete();
  }
}
