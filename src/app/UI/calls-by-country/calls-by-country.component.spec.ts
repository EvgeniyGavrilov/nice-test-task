import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CallsByCountryComponent } from './calls-by-country.component';

describe('CallsByCountryComponent', () => {
  let component: CallsByCountryComponent;
  let fixture: ComponentFixture<CallsByCountryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CallsByCountryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CallsByCountryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
