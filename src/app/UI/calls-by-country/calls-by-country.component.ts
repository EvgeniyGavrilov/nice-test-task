import {Component, Input} from '@angular/core';
import {CallsByCountryMap} from "@types";


@Component({
  selector: 'app-calls-by-country[callsByCountry]',
  templateUrl: './calls-by-country.component.html',
  styleUrls: ['./calls-by-country.component.scss']
})
export class CallsByCountryComponent {
  @Input() callsByCountry!: CallsByCountryMap;
}
