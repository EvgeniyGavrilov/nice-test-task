import {Component, Input} from '@angular/core';


@Component({
  selector: 'app-agents',
  templateUrl: './agents.component.html',
  styleUrls: ['./agents.component.scss'],
})
export class AgentsComponent {
  @Input() agents: number = 0;
}
