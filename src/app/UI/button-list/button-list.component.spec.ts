import { ComponentFixture, TestBed } from '@angular/core/testing';

import {ButtonListComponent} from './button-list.component';
import {MOCK_DATA} from "../../app.component";
import {By} from "@angular/platform-browser";
import {DebugElement, NO_ERRORS_SCHEMA} from "@angular/core";
import {Status} from "@enums";

describe('ButtonListComponent', () => {
  let component: ButtonListComponent;
  let fixture: ComponentFixture<ButtonListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonListComponent ],
      schemas: [NO_ERRORS_SCHEMA],
    })
    .compileComponents();

    fixture = TestBed.createComponent(ButtonListComponent);
    component = fixture.componentInstance;
    component.settings = [
      {
        isActive: true,
        status: Status["Processed"],
        counter: 0,
      },
      {
        isActive: true,
        status: Status["Active"],
        counter: 0,
      },
      {
        isActive: true,
        status: Status["Waiting"],
        counter: 0,
      },
      {
        isActive: true,
        status: Status["Hang-Up"],
        counter: 0,
      }
    ];
    fixture.detectChanges();
    component.itemsDataOrigin = MOCK_DATA;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update settings', () => {
    expect(component.settings.length).toEqual(4);
  });

  it('should create buttons', () => {
    const { debugElement } = fixture;
    const buttons = debugElement.queryAll(By.css('app-button'));
    expect(buttons.length).toEqual(4);
  });

  it('should be called', () => {
    const emitFiltersSpy = spyOn(component.filtersWasChanged, 'emit')
    const { debugElement } = fixture;
    const button: DebugElement = debugElement.query(By.css('app-button'));
    button.triggerEventHandler('valueWasChanged');
    expect(emitFiltersSpy).toHaveBeenCalled();
  });
});
