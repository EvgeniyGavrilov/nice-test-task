import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ButtonsListSettings} from "@types";
import {DataItem} from "@interfaces";


@Component({
  selector: 'app-button-list[settings][itemsDataOrigin]',
  templateUrl: './button-list.component.html',
  styleUrls: ['./button-list.component.scss']
})
export class ButtonListComponent {
  @Output() filtersWasChanged: EventEmitter<void> = new EventEmitter();
  @Input() settings: ButtonsListSettings = [];
  @Input() set itemsDataOrigin(items: DataItem[]) {
    this.settings = items.reduce((acc, item) => {
      const filterSetting = acc.findIndex(el => el.status === item.status);
      if (filterSetting >= 0) {
        acc[filterSetting].counter += 1;
      }
      return acc
    }, this.settings.map(el => {
      el.counter = 0;
      return el
    }))
  }
}
