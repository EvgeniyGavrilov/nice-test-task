import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './button/button.component';
import { ButtonListComponent } from './button-list/button-list.component';
import { AgentsComponent } from './agents/agents.component';
import { CallsByCountryComponent } from "./calls-by-country/calls-by-country.component";
import { ReactiveFormsModule } from "@angular/forms";




@NgModule({
  declarations: [
    ButtonComponent,
    AgentsComponent,
    ButtonListComponent,
    CallsByCountryComponent
  ],
  exports: [
    AgentsComponent,
    CallsByCountryComponent,
    ButtonListComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ]
})
export class UIModule { }
