import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ButtonComponent} from './button.component';
import {Status} from "@enums";

const MOCK_FILTER_SETTING = {
  value: true,
  status: Status.Active,
  counter: 0
}
describe('ButtonComponent', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;
    component.setting = {
      ...MOCK_FILTER_SETTING
    }
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit event', () => {
    const emitFiltersSpy = spyOn(component.valueWasChanged, 'emit')
    component.sendEvent();
    expect(emitFiltersSpy).toHaveBeenCalled();
  });

  it('should change value for button status', () => {
    component.sendEvent();
    fixture.detectChanges();
    expect(component.setting.isActive).not.toEqual(MOCK_FILTER_SETTING.value);
  });

  it('should display right name', () => {
    const name = component.getName(1);
    fixture.detectChanges();
    expect(name).toEqual('Active');
  });
});
