import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FilterSettings} from "@interfaces";
import {Status} from "@enums";



@Component({
  selector: 'app-button[setting]',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() setting!: FilterSettings;
  @Output() valueWasChanged: EventEmitter<void> = new EventEmitter<void>();
  constructor() { }

  ngOnInit(): void {}

  sendEvent() {
    this.setting.isActive = !this.setting.isActive
    this.valueWasChanged.emit();
  }

  getName(status: Status): string {
    return Status[status] ? Status[status] : '';
  }
}
