import {Status} from "@enums";

export interface DataItem {
  agentId: string,
  timestamp: number,
  duration: number
  country: string,
  status: Status
}
