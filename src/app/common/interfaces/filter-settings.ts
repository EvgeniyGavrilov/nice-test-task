import {Status} from "../enums";

export interface FilterSettings {
  status: Status,
  isActive: boolean,
  counter: number
}
