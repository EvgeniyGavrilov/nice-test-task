import {DataItem} from "@interfaces";

export type CallsByCountryMap = Map<string, DataItem[]>
