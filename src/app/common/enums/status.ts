export enum Status {
  'Processed',
  'Active',
  'Waiting',
  'Hang-Up'
}
